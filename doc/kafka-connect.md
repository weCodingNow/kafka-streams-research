https://medium.com/@stephane.maarek/the-kafka-api-battle-producer-vs-consumer-vs-kafka-connect-vs-kafka-streams-vs-ksql-ef584274c1e

### Что такое

**Kafka Connect Source API**

The Kafka Connect Source API is a whole framework built on top of the Producer API. It was built so that developers would get a nicer API made for 1) producer tasks distribution for parallel processing, and 2) easy mechanism to resume your producers. The final goodie is a bustling variety of available connectors you can leverage today to onboard data from most of your sources, without writing a single line of code.


### Подводные камни

дебаггинг:

https://stackoverflow.com/questions/45717658/what-is-a-simple-effective-way-to-debug-custom-kafka-connectors/45719652#45719652
