https://simplydistributed.wordpress.com/2017/03/21/kafka-streams-state-stores/

### Что это такое

Хранилище данных в формате ключ-значение для приложения, использующего Kafka Streams API

Оно хранит данные для партиции, за которое отвечает данный инстанс приложения

Оно нужно для кеширования результатов вычислений (основное хранилище данных для приложения - сжатый топик в Kafka(Compacted Log))

### Особенности

### Подводные камни


### Кастомные хранилища данных

https://docs.confluent.io/current/streams/developer-guide/processor-api.html

Есть два способа интерпретации понятия "кастомное хранилище данных":

Хранилище данных, которое создаётся не для хранения какого-то результата вычисления (например count()), а просто, например, при инициализации процессора или приложения, и это просто обычное хранилище данных, которое использует RocksDB.

Это делается так:

```java
StoreBuilder countStoreBuilder =
  Stores.keyValueStoreBuilder(
    Stores.persistentKeyValueStore("persistent-counts"),
    Serdes.String(),
    Serdes.Long()
  );
```

Но можно и запилить свой тип хранилища.

Что (предположительно) нужно курить чтобы приобрести знание о том как написать свой класс для хранения данных

https://docs.confluent.io/current/streams/developer-guide/processor-api.html#streams-developer-guide-state-store-custom
https://kafka.apache.org/23/javadoc/org/apache/kafka/streams/processor/StateStore.html
https://kafka.apache.org/23/javadoc/org/apache/kafka/streams/state/KeyValueStore.html
