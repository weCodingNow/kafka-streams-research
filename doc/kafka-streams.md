## Kafka

Apache Kafka - distributed streaming platform

Предоставляет три возможности:

* Публикация и подписка на потоки записей
* Устойчивое к приколам хранение потоков записей
* Преобразование потоков записей с их появлением

Основные задачи, решаемые Kafka:

* Выступает медиатором в общении микросервисов

* Построение работающих с потоками приложений, которые реагируют на и преобразовывают потоки данных

## Kafka Streams

Kafka Streams is a client library for building mission-critical real-time applications and microservices, where the input and/or output data is stored in Kafka clusters.

Т.к. это либа, код, использующий Kafka Streams, работает сам по себе, а не исполняется где-то

Абстракция над Kafka Consumers/Producers, т.е. если мы создаём приложение, работающее с Kafka Streams, то, например, входной поток данных представляет собой один из потребителей в группе потребителей

Работа этого АПИ заключается в построении **топологии**, описывающей логику совершения вычислений приложением. **Топология** выглядит как граф из узлов - **процессоров**, каждый из которых совершает какое-то преобразование входных данных перед передачей данных далее в графе. В **топологии** специальное место занимают два процессора - **Source processor** (узел, который вводит данные в граф, занимается чтением из топика кафки) и **Sink processor** (узел, который выводит данные из графа, либо засовывая их в топик кафки, либо в какой-то другой потребитель данных, подключенный через Kafka Connect).

![](topology.jpg)

### Зачем оно нужно

If you want to get into the stream processing world, meaning reading data from Kafka in real-time and after processing it, writing it back to Kafka, you would most likely pull your hair out if you used the Kafka Consumer API chained with the Kafka Producer API. Thankfully, the Kafka project now ships with Kafka Streams API (available for Java and Scala), which enables you to write either a High Level DSL (resembling to a functional programming / Apache Spark type of program), or the Low Level API (resembling more to Apache Storm). The Kafka Streams API does require you to code, but completely hides the complexity of maintaining producers and consumers, allowing you to focus on the logic of your stream processors. It also comes with joins, aggregations and exactly-once processing capabilities!(https://medium.com/@stephane.maarek/the-kafka-api-battle-producer-vs-consumer-vs-kafka-connect-vs-kafka-streams-vs-ksql-ef584274c1e)

https://blog.cloudera.com/kafka-streams-is-it-the-right-stream-processing-engine-for-you/

Kafka Streams API нужен для легкого написания приложений, которые легко скейлятся и занимаются преобразованием потоков данных

Это API подходит для разработки в случае, если:

* Your stream processing application consists of Kafka to Kafka pipelines.
* You don’t need/want another cluster for stream processing.
* You want to perform common stream processing functions like filtering, joins, aggregations, enrichments on the stream for simpler stream processing apps.
* Your target users are developers with java dev backgrounds.
* Your use cases are about building lightweight microservices, simple ETL and stream analytics apps.

### Особенности

- Java/Scala only
- Kafka Streams API это библиотека, т.е. приложение, написанное с использованием Kafka Streams API не нужно каким-то специальным образом деплоить - чтобы начать обрабатывать данные из топика кафки достаточно просто запустить приложение
- Простой скейлинг - нужно больше вычислений - запускаешь ещё инстанс приложения
- Есть два способа использования Kafka Streams:
  - Высокоуровневый: Kafka Streams DSL (https://docs.confluent.io/current/streams/developer-guide/dsl-api.html)
  - Processor API (https://docs.confluent.io/current/streams/developer-guide/processor-api.html#streams-developer-guide-processor-api)
- По-умолчанию нет сериализаторов/десериализаторов JSON; Есть только для примитивных типов типа чисел, строк
- Позволяет создавать хранилища данных для stateful вычислений, которым требуется знать какое-то состояние
- Эти хранилища данных могут быть реплицированы


### Решаемые задачи

* Потоковая обработка данных из Kafka через выстроение цепочки обработки данных
* Агрегация потоков данных (всякие join'ы, фильтры, обогащение данных с помощью соединения разных потоков и т.д.)
* Сбор метрики (приложение, использующее Kafka Streams, не обязательно должно перерабатывать данные - оно может их просто потреблять, т.е. получать данные из топика, но не пушить данные в другой топик)

### Use Case'ы

* (Kafka) Как основа для Event-Driven архитектуры

https://content.pivotal.io/intersect/introduction-to-event-driven-architecture-and-apache-kafka


### Перф

<!-- не мог найти инфы конкретно про Kafka Streams, но моё предположение что приложение, использующее Kafka Streams, будет настолько же производительно, насколько явлеяется производительным Apache Kafka и само приложение

 -->


### Ключевые понятия
https://docs.confluent.io/current/streams/concepts.html

**Stream** (Поток) - неограниченный, постоянно обновляющийся набор данных. Состоит из собственного топика и как минимум одной партиции в этом топике.

**Data record** (Запись) - пара ключ-значение. Как бы типизированная, но для кафки и для приложения выглядит просто как набор байтов, поэтому для работы с записями нужно использовать сериализаторы-десериализаторы.

**Stream processing application** (Приложение) - любая программа, использующая Kafka Streams API.

**Application instance** (Инстанс) - запущенный процесс приложения.

**Topology** (Топология) - вычислительный граф. Каждый узел в графе является **Процессором** и определяет определённый "шаг" в обработке данных, или, иными словами, то - каким образом нужно преобразовать поток данных.

**Stream processor**(Процессор) - узел в топологии. Представляет собой "шаг" в обработке данных в топологии, или, иными словами, определённое преобразование данных из потока/нескольких потоков.

Примеры **Процессоров**: операции map(), filter(), join(), count().

**Stateless stream processing** - такой вид работы приложения, при котором для обработки одной записи из входного потока данных нет необходимости знать что-либо о других записях данных в этом или любом другом потоке.

**Stateful stream processing** - такой вид работы приложения, при котором для корректной обработки входного потока данных необходимо знать о состоянии других записей в этом или другом потоке данных.

Примеры операций, при выполнении которых приложению необходимо знать более чем об одной записи за раз:

join - объединение записей из нескольких потоков по ключу

KStream stream1 = (1: 'a', 2: 'b', 3: 'c', ...)
KStream stream2 = (1: 'A', 2: 'B', 4: 'D', ...)

stream1 join stream2 = (1: 'a - A', 2: 'b - B', ...)

aggregation - получение из потока данных или таблицы новой таблицы, которая состоит из как-то объединённых записей источника

Пример - подсчёт количества записей по ключу.

При произведении stateful stream processing операций происходит две вещи:

* состояние преобразований записывается в специальные топики кафки (changelog topics)

* состояние преобразований кешируется в локальном для инстансов приложений хранилище, которое своим бэкендом имеет RocksDB

### KStream vs KTable

**KStream** - Абстракция над потоком записей; Каждая запись в этом потоке данных означает уникальную точку данных, определённую запись вида ключ-значение

В KStream все точки данных существуют одновременно - т.е. пример:
есть поток записей данных('a': 1, 'a': 2, 'a': 3)
приложение суммирует все числа в потоке данных по ключу 'a', на выходе получается 1 + 2 + 3 = 6

**KTable** - Абстракция над потоком изменений; Каждая запись в этом потоке означает последнее изменение записи определённому ключу

т.е. пример:
есть поток записей данных ('a': 1, 'a': 2, 'a': 3)
Мы из этого потока делаем KTable и суммируем все записи по ключу 'a', на выходе получается 3 (т.к. в таблице по ключу 'a' будет храниться последнее в потоке значение - 3)


### Почему (субъективно) так долго происходит работа с stateful процессором

потому что по умолчанию, если приложение выполняет stateful stream processing, ставится высокий COMMIT_LATENCY - я так понимаю, это сделано потому, что результатом работы таких приложений часто бывают какие-то сложные агрегаты/объекты/т.д., и поэтому на одну новую запись во входном потоке данных обычно не приходится 1 сложный результат работы такого приложения

Т.е. в примере работы приложения, подсчитывающего количество букв во всех сообщениях, когда-либо пришедших во входной топик, приложение сначала в течение какого-то интервала времени считывает входные сообщения и только потом выдаёт сообщение, которое показывает сколько раз какая буква употреблялась


### Local state, remote state

https://docs.confluent.io/current/streams/developer-guide/interactive-queries.html

см kafka_localstore.md

### Interactive Query

https://docs.confluent.io/current/streams/developer-guide/interactive-queries.html

![](IQ.png)

Interactive Query - API для того, чтобы обращаться к данным приложения, если оно сохраняет какие-то данные

Т.е. например есть код:

```java
KGroupedStream<String, String> groupedByWord = textLines
  .flatMapValues(value -> Arrays.asList(value.toLowerCase().split("\\W+")))
  .groupBy((key, word) -> word, Grouped.with(stringSerde, stringSerde));

// Create a key-value store named "CountsKeyValueStore" for the all-time word counts
groupedByWord.count(Materialized.<String, String, KeyValueStore<Bytes, byte[]>as("CountsKeyValueStore"));
```

В данном примере идёт количества уникальных слов, слово - будет ключ, его количество - значение.

И эта таблица сохраняется в локальном хранилище под названием "CountsKeyValueStore".

Чтобы получить данные из локального хранилища приложения:

```java
// Get the key-value store CountsKeyValueStore
ReadOnlyKeyValueStore<String, Long> keyValueStore =
    streams.store("CountsKeyValueStore", QueryableStoreTypes.keyValueStore());
 
// Get value by key
System.out.println("count for hello:" + keyValueStore.get("hello"));
 
// Get the values for a range of keys available in this application instance
KeyValueIterator<String, Long> range = keyValueStore.range("all", "streams");
while (range.hasNext()) {
  KeyValue<String, Long> next = range.next();
  System.out.println("count for " + next.key + ": " + value);
}
 
// Get the values for all of the keys available in this application instance
KeyValueIterator<String, Long> range = keyValueStore.all();
while (range.hasNext()) {
  KeyValue<String, Long> next = range.next();
  System.out.println("count for " + next.key + ": " + value);
}

```


https://docs.confluent.io/current/streams/developer-guide/interactive-queries.html

### Materialized --- ?

При создании какого-то агрегата данных (например count) можно указать, что данные сохраняются в каком-то хранилище данных, для чего и нужен тип 
```java
Materalized<K, V, S>(%название хранилища данных%)
```

https://docs.confluent.io/current/streams/developer-guide/interactive-queries.html


### Консистентность

https://dev.to/barryosull/event-sourcing-what-it-is-and-why-its-awesome

статья от сбера https://habr.com/ru/company/sberbank/blog/353608/



### Exactly once

https://www.confluent.io/blog/enabling-exactly-once-kafka-streams/

https://medium.com/@andy.bryant/processing-guarantees-in-kafka-12dd2e30be0e <-- тут описан механизм работы гарантии Exactlty once в Kafka Streams

При создании приложения можно настроить т.н. processing guarantee - в зависимости от неё кафка-потребители, которые создаёт Kafka Streams API, будут обрабатывать каждое сообщение из топика, представляемого как входной поток данных:

* No guarantee - без гарантий на обработку данных ("no_guarantee") - т.е. не обязательно, что каждое сообщение будет обработано хотя бы раз каждым потребителем

* At most once - с гарантией, что каждое сообщение будет обработано максимум один раз (т.е. допустимо такое, что сообщение будет игнорировано, но если оно будет прочитано одним из потребителей в группе потребителей приложения, то только одним потребителем и один раз)

* At least once - гарантия, что каждое сообщение будет обработано 1 и более раз каким-то потребителем

**Exactly once** - гарантия, что каждое сообщение будет прочитано ровно один раз ровно одним потребителем

### Processor API

https://docs.confluent.io/current/streams/developer-guide/processor-api.html
https://docs.confluent.io/current/streams/javadocs/index.html

Processor API позволяет разработать свои собственные узлы обработки данных (Процессоры), которые могут взаимодействовать c хранилищем данных инстанса приложения (см kafka-localstore.md)

Процессоры обрабатывают по записи за раз.

Процессор это класс, имплементирующий интерфейс Processor<тип входных данных, тип выходных данных>


### KSQL

KSQL is not directly part of the Kafka API, but a wrapper on top of Kafka Streams.

https://docs.confluent.io/current/ksql/docs/index.html

https://www.confluent.io/stream-processing-cookbook/ksql-recipes/setting-kafka-message-key/


### Примеры из блогов

Построение Event-Sourced системы с Kafka Streams: https://www.infoq.com/news/2018/07/event-sourcing-kafka-streams/

бэйсикли Event-Sourced система это такая система, в которой нет хранилища данных для какого-то состояния; Если нужно взглянуть на состояние системы в данный момент, можно по записям об изменении системы в течение всего её времени существования выстроить состояние системы в данный момент, и уже это представление кешировать где-то

неконкретная статья от Deutsche Bahn: https://www.confluent.io/blog/deutsche-bahn-kafka-and-confluent-use-case/

статья от сбера https://habr.com/ru/company/sberbank/blog/353608/

### Кейсы для телекома

(-_-)

### Интересные источники

* https://www.imperva.com/blog/not-just-for-processing-how-kafka-streams-as-a-distributed-database-boosted-our-reliability-and-reduced-maintenance/


### Что почитать

- Книга "Kafka Streams in Action"
- https://docs.confluent.io/current/streams/
- https://habr.com/ru/company/skbkontur/blog/353204/
- https://kafka.apache.org/23/documentation/streams/tutorial
- https://hackernoon.com/best-practices-for-event-driven-microservice-architecture-e034p21lk


### Что посмотреть

* Виктор Гамов, Confluent - Kafka Streams IQ: "Зачем нам база данных?" https://www.youtube.com/watch?v=PgoRuxKqStI
* Kafka Streams in Production - https://www.youtube.com/watch?v=ZnGgG03DVU8
* Kafka Streaming Platform Explained - https://www.youtube.com/watch?v=A9KQufewd-s&feature=youtu.be&list=PLAdzTan_eSPQsR_aqYBQxpYTEQZnjhTN6


### Кто использует

Не нашёл пользователей среди российских компаний

Из забугорных

* The New York Times
* Pinteres
* Rabobank
  * https://www.confluent.io/blog/real-time-financial-alerts-rabobank-apache-kafkas-streams-api
  * https://github.com/Axual/rabo-alerts-blog-post/blob/master/src/main/java/BalanceAlertsTopology.java

