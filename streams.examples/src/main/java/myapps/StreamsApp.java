package myapps;

import java.util.Properties;
import java.util.concurrent.CountDownLatch;

import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.Topology;

public abstract class StreamsApp {
    KafkaStreams kstreams;


    public abstract void createTopology(StreamsBuilder builder);

    // этот метод для настройки приложения
    // тут задаются всякие параметры типа кол-ва тредов и т.д.
    public abstract void configurate(Properties props);

    Topology startTopologyCreation() {
        final StreamsBuilder builder = new StreamsBuilder();

        this.createTopology(builder);

        return builder.build();
    }

    Properties startConfiguration() {
        Properties props = new Properties();

        // System.out.println("coolest debug ever");
        // System.out.println(this.getClass().toString().split("\\.")[1]);

        props.put(
            StreamsConfig.APPLICATION_ID_CONFIG,
            this.getClass().toString().toLowerCase().split("\\.")[1]
        );

        this.configurate(props);

        return props;
    }


    public void start() {
        this.gracefulStartup(
            new KafkaStreams(
                this.startTopologyCreation(),
                this.startConfiguration()
            )
        );
    }

    void gracefulStartup(KafkaStreams ks) {
        final CountDownLatch latch = new CountDownLatch(1);

        Runtime.getRuntime().addShutdownHook(new Thread("streams-wc-shutdown-hook") {
            @Override
            public void run() {
                ks.close();
                latch.countDown();
            }
        });

        try {
            ks.start();
            latch.await();
        } catch (final Throwable e) {
            System.exit(1);
        }

        System.exit(0);
    }
}
