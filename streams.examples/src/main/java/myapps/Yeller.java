package myapps;

import java.util.Properties;

import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.kstream.Consumed;

public class Yeller extends StreamsApp {
    public void configurate(Properties props) {
        props.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");

        props.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.String().getClass());
        props.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, Serdes.String().getClass());
    }

    public void createTopology(StreamsBuilder builder) {
        builder.stream("streams-plaintext-input", Consumed.with(Serdes.String(), Serdes.String()))
            .mapValues(value -> value.toUpperCase())
            .to("streams-yeller-output");
    }

    public static void main(String[] args) {
        Yeller yeller = new Yeller();
        yeller.start();
    }
}
